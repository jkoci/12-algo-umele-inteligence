#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Provides tree structure for MCTS
"""
from math import sqrt, log


class Node:
    """
    Node for MCTS tree
    """
    def __init__(self, move=None, parent=None, state=None):
        self.move = move  # which move created this state
        self.parent = parent  # Root node has no parent
        self.child = []
        self.wins = 0
        self.total = 0
        self.available_moves = state.get_moves()

    def select_child(self):
        """ Selects best child while travelling down the tree """
        sel = sorted(self.child,
                     key=lambda c: c.wins/c.total
                     + sqrt(2*log(self.total)/c.total))[-1]
        # print("Selected:", sel)
        # print("at address ", hex(id(sel)))
        return sel

    def make_child(self, cmove, cstate):
        """ Creates offspring from current state and selected move """
        new = Node(cmove, self, cstate)
        self.available_moves.remove(cmove)
        self.child.append(new)
        return new

    def update(self, wins):
        """ Updates node ratio while back-propagating """
        self.total += 1
        self.wins += wins

    def __str__(self):
        move = self.move if self.move is not None else "nil"
        ret = "Node ---"
        ret += "\n  Move: "+str(move)
        ret += "\n  Childs: "+str(self.child)
        ret += "\n  Avail: "+str(self.available_moves)
        ret += "\n  W/T rat: {}/{}".format(self.wins, self.total)

        return ret
