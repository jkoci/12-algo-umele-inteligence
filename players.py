#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Provides some players for Connect-four game.
"""
import random
import datetime
import copy

from node import Node


class Player():
    """
    Generic player class for Connect-four
    """
    @classmethod
    def cnt(cls):
        """ Unnamed counter """
        cls._cnt += 1
        return cls._cnt

    _cnt = 0

    def __init__(self, symbol, **kwargs):
        self.symbol = symbol
        self.name = kwargs.get('name',
                               'Unnamed {}'.format(Player.cnt()))

    def __str__(self):
        return "{} ({})".format(self.name, self.symbol)

    def play(self, _):
        """ Implement this method to allow playing """
        raise NotImplementedError("play method not implemented")


class RandomPlayer(Player):
    """
    Randomly playing opponent for Connect-four
    """
    def __init__(self, symbol, **kwargs):
        # pylint: disable=W0235
        # For clairy and extensibility sake, super()'s init is called
        super().__init__(symbol, **kwargs)
        # pylint: enable=W0235

    def play(self, board):
        """ Randomly select possible move"""
        legal = board.get_moves()
        return random.choice(legal)


class HumanPlayer(Player):
    """
    Human player interface for Connect-four
    """
    def __init__(self, symbol, **kwargs):
        # pylint: disable=W0235
        # For clairy and extensibility sake, super()'s init is called
        super().__init__(symbol, **kwargs)
        # pylint: enable=W0235

    def play(self, _):
        """
        Provides human-keyboard interface
        """
        move = input("{} plays > ".format(self.name))
        return move


class MCPlayer(Player):
    """
    Implements MonteCarloTreeSearch method for playing Connect-four.
    """
    def __init__(self, symbol, **kwargs):
        super().__init__(symbol, **kwargs)
        self.stupidity = kwargs.get('stupidity', 0.05)
        self.maxiter = kwargs.get('maxiter', 3000)
        self.time = datetime.timedelta(seconds=kwargs.get('time', 5))

    def play(self, board):
        """
        Selects game move - according to MonteCarloTreeSearch
        """
        result = random.choice(board.get_moves())
        if random.uniform(0, 1) < self.stupidity:
            return result

        hallucination = copy.deepcopy(board)
        root = Node(state=hallucination)
        for _ in range(self.maxiter):
            node = root
            state = copy.deepcopy(hallucination)

            # selecting best "known" node
            while node.available_moves is not None and \
                    node.child != []:
                node = node.select_child()
                state.play(node.move)

            # We've reached last fully "known" node
            if node.available_moves is not None:
                move = random.choice(node.available_moves)
                state.play(move)
                node = node.make_child(move, state)

            # Start MonteCarlo-ing
            while state.get_moves() is not None:
                state.play(random.choice(state.get_moves()))

            # We've finished - now back propagate
            won = state.score
            while node is not None:  # until reach root
                node.update(won)
                node = node.parent

        best = root.select_child()
        return best.move
