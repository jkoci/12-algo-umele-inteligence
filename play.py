#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Connect-4 game. Includes Human interface, MonteCarlo AI and Random "AI".
"""
# pylint: disable=W0611
# I'm sick of uncommenting stuff every time I change players
from players import HumanPlayer, RandomPlayer, MCPlayer
# pylint: enable=W0611
from game import GameBoard


def _main():

    alice = MCPlayer("X", name="McAlice", stupidity=0.3)
    # alice = HumanPlayer("*", name="Alice")
    # alice = RandomPlayer("#")
    bob = MCPlayer("0", name="MC Bob")

    board = GameBoard(cols=7, rows=6)
    board.player_one = alice  # player one starts the game
    board.player_two = bob

    while board.available_moves != 0:
        print(board)
        if board.player is board.player_one:
            while True:
                move = board.player_one.play(board)
                if board.is_legal(move):
                    break
        else:
            while True:
                move = board.player_two.play(board)
                if board.is_legal(move):
                    break

        print("{} played {}".format(board.player.name, move))
        board.play(move)
        won = board.game_won()
        if won is not None:
            break

    print(board)
    if board.score == 1:
        print(alice, "wins!")
    elif board.score == -1:
        print(bob, "wins!")
    else:
        print("It's a draw!")


if __name__ == "__main__":
    _main()
