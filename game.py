#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Game board and rules for Connect-four game.
"""

SHIFTS = [[-1, -1], [-1, 0], [-1, 1],
          [0, -1], [0, 1],  # shift [0, 0] has no sense
          [1, -1], [1, 0], [1, 1]]
EMPTY = "."


class GameBoard():
    """
    Game board. Governs player turns, legal moves and winning conditions.
    """
    def __init__(self, cols, rows):
        self.board = [[EMPTY for c in range(cols)]
                      for r in range(rows)]
        self.rows = rows
        self.cols = cols

        self.player_one = None
        self.player_two = None

        self._player = None

    @property
    def player(self):
        """ This property contains pointer to player currently to move """
        return self._player or self.player_one

    @player.setter
    def player(self, val):
        self._player = val

    def game_won(self):
        """
        Determines if the game finised by winning.
        @returns winning symbol or None, if game is to be continued
        """
        for row in range(self.rows-1, -1, -1):
            for col in range(self.cols-1, -1, -1):
                coords = [row, col]
                item = self[coords]
                if item == EMPTY:
                    continue
                won = self._search_for_winner(coords)
                if won:
                    return item
        return None

    def _search_for_winner(self, center):
        won = False
        item = self[center]
        for shf in SHIFTS:
            for dist in range(4):
                shift = [x*dist for x in shf]
                pos = [sum(x) for x in zip(center, shift)]
                if not self._pos_is_valid(pos):
                    break
                if item != self[pos]:
                    break
                if dist == 3:
                    won = True
                    break
            if won:
                break

        return won

    def _pos_is_valid(self, pos):
        row = pos[0]
        col = pos[1]
        if row < 0 or row >= self.rows or \
           col < 0 or col >= self.cols:
            return False

        return True

    def top_of_column(self, col):
        """
        Returns top empty field in column col
        (or None, if the column is filled up)
        """
        ret = None
        for row in range(self.rows-1, -1, -1):
            if self[row, col] == EMPTY:
                ret = row
                break
        return ret

    def is_legal(self, move):
        """
        Checking, if the move is legal
         - selected column must be non-full
        """
        # assert move == int(move), "Move must be integer"
        return self.top_of_column(move) is not None

    def get_moves(self):
        """
        Provides list of legal moves
        """
        moves = list()
        if self.game_won():
            return None

        for move in range(self.cols):
            if self.is_legal(move):
                moves.append(move)
        if not moves:
            return None
        return moves

    @property
    def available_moves(self):
        """
        Returns number of available moves
        """
        return len(self.get_moves())

    def play(self, move):
        """
        Plays selected move
        """
        toc = self.top_of_column(move)
        assert toc is not None, \
            "Attempting to play illegal move {}\n{}\n{}"\
            .format(move, "-"*7, self)
        self[toc, move] = self.player.symbol

        if self.player is self.player_one:
            self.player = self.player_two
        else:
            self.player = self.player_one

    @property
    def score(self):
        """
        Returns current board score
         - 1 if player one won
         - -1 if player two won
         - 0 if draw/not won yet
        """
        won = self.game_won()
        if won == self.player_one.symbol:
            ret = 1
        elif won == self.player_two.symbol:
            ret = -1
        else:
            ret = 0
        return ret

    def __str__(self):
        ret = ""
        for col in range(self.cols):
            ret += str(col)

        ret += "\n" + "="*self.cols + "\n"

        for row in range(self.rows):
            for col in range(self.cols):
                ret += self.board[row][col]
            ret += "\n"

        return ret

    def __getitem__(self, key):
        assert len(key) == 2, "Subscript not in [x, y] shape"
        x = int(key[0])
        y = int(key[1])
        return self.board[x][y]

    def __setitem__(self, key, val):
        assert len(key) == 2, "Subscript not in [x, y] shape"
        x = int(key[0])
        y = int(key[1])
        self.board[x][y] = val
